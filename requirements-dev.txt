coverage==6.0.2
flake8==4.0.1
flake8-docstrings==1.6.0
mypy==0.910
pytest==6.2.5
