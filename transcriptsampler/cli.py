"""Command-line interface client."""

import argparse
from enum import Enum
import logging
from pathlib import Path
import signal
import sys

from transcriptsampler import __version__
from transcriptsampler.transcript_sampler import TranscriptSampler as ts

LOG = logging.getLogger(__name__)


class LogLevels(Enum):
    """Log level enumerator."""

    DEBUG = logging.DEBUG
    INFO = logging.INFO
    WARN = logging.WARNING
    WARNING = logging.WARNING
    ERROR = logging.ERROR
    CRITICAL = logging.CRITICAL


def parse_args() -> argparse.Namespace:
    """Parse CLI arguments.

    Returns:
        Parsed CLI arguments.
    """
    # set metadata
    description = (
        f"{sys.modules[__name__].__doc__}\n\n"
        ""
    )
    epilog = (
        f"%(prog)s v{__version__}, (c) 2021 by Zavolab "
        "(zavolab-biozentrum@unibas.ch)"
    )

    # instantiate parser
    parser = argparse.ArgumentParser(
        description=description,
        epilog=epilog,
        add_help=False,
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )

    # add arguments
    parser.add_argument(
        'input_file',
        type=lambda p: Path(p).absolute(),
        metavar="PATH",
        help="path to tab-separated file with gene abundances per gene",
    )
    parser.add_argument(
        "--output-file",
        default=Path.cwd() / 'results.tsv',
        type=lambda p: Path(p).absolute(),
        metavar="PATH",
        help="path to file where output is written to in tab-separated form",
    )
    parser.add_argument(
        "--num-transcripts",
        default=0,
        type=int,
        metavar="INT",
        help="number of transcripts to sample",
    )
    parser.add_argument(
        "--verbosity",
        choices=[e.name for e in LogLevels],
        default=LogLevels.INFO.name,
        type=str,
        help="logging verbosity level",
    )
    parser.add_argument(
        "-h", "--help",
        action="help",
        help="show this help message and exit",
    )
    parser.add_argument(
        "--version",
        action="version",
        version=epilog,
        help="show version information and exit",
    )

    # return parsed arguments
    return parser.parse_args()


def setup_logging(verbosity: str = 'INFO') -> None:
    """Configure logging.

    Args:
        verbosity: Level of logging verbosity.
    """
    level = LogLevels[verbosity].value
    logging.basicConfig(
        level=level,
        format="[%(asctime)s %(levelname)s] %(message)s",
        datefmt='%Y-%m-%d %H:%M:%S',
    )


def main() -> None:
    """Entry point for CLI executable."""
    try:
        # handle CLI args
        args = parse_args()

        # set up logging
        setup_logging(verbosity=args.verbosity)
        LOG.info("Started transcript sampler...")
        LOG.debug(f"CLI arguments: {args}")

        sampler = ts(input_file=args.input_file)
        sampler.read_avg_expression()
        sampler.sample_transcripts(num_transcripts=args.num_transcripts)
        sampler.write_sample(file_name=args.output_file)

    except KeyboardInterrupt:
        LOG.error('Execution interrupted.')
        sys.exit(128 + signal.SIGINT)

    # conclude execution
    LOG.info("Done.")


if __name__ == '__main__':
    main()
