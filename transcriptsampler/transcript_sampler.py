"""Package containing utilities for simulating single cell RNA sequencing.

Class:
    TranscriptSampler: contains all methods
"""

import logging
from pathlib import Path
from random import choices
from typing import Dict

LOG = logging.getLogger(__name__)


class TranscriptSampler:
    """Collection of functions for simulating single cell RNA sequencing.

    Args:
        input_file: Path to file with gene abundances.

    Attributes:
        input_file: Path to file tab-separated file with gene abundances per
            gene.
        gene_expression: Dictionary of gene name and average expression.
        transcripts_per_gene: Dictionary of gene name and number of
            transcripts.
    """

    def __init__(
        self,
        input_file: Path,
    ) -> None:
        """Class constructor."""
        self.input_file: Path = input_file
        self.gene_expression: Dict = {}
        self.transcripts_per_gene: Dict = {}

    def read_avg_expression(self) -> None:
        """Convert file with gene expression to dictionary."""
        LOG.info("Reading input file...")
        with open(self.input_file) as myfile:
            for line in myfile:
                gene_id, nr_copies = line.split('\t')
                self.gene_expression[gene_id] = int(nr_copies)
        LOG.info("Input file read.")

    def sample_transcripts(self, num_transcripts: int = 1000) -> None:
        """Sample transcripts from a dictionary.

        Args:
            num_transcripts: Number of samplings.
        """
        LOG.debug("Sampling transcripts...")

        # list where each gene is entered n times, where n is the average
        # expression
        list_genes_to_pick = []
        for gene_key in self.gene_expression.keys():
            for _ in range(0, self.gene_expression[gene_key]):
                list_genes_to_pick.append(gene_key)

        # initialize gene expression dictionary
        self.transcripts_per_gene = self.gene_expression.copy()
        for gene_key in self.transcripts_per_gene:
            self.transcripts_per_gene[gene_key] = 0

        # randomly chose which genes to transcribe.
        for _ in range(0, num_transcripts):
            transcript = choices(list_genes_to_pick)[0]
            self.transcripts_per_gene[transcript] += 1

        LOG.debug("Transcripts sampled.")

    def write_sample(
        self,
        file_name: Path = Path.cwd() / 'results.tsv',
    ) -> None:
        """Save the sampled transcript dictionary to a file.

        Args:
            file_name: Path to file where output is written to in tab-separted
                form.
        """
        LOG.info("Writing output file...")
        with open(file_name, "w") as myfile:
            for key in self.transcripts_per_gene.keys():
                text = "".join([
                    key,
                    '\t',
                    str(self.transcripts_per_gene[key]), "\r",
                ])
                myfile.write(text)
        LOG.info("Output written.")
